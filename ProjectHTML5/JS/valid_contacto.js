$(document).ready(function(){ 
  	var nameReg = /^[A-Za-z]+$/;
    var numberReg = /^([0-9]+){7,10}$/;
    var emailReg = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    
    $('#Nombre').blur(function() {
		var is_name = nameReg.test($(this).val());
        if(is_name){
            $(this).removeClass("invalid").addClass("valid");
        }
        else{
            $(this).removeClass("valid").addClass("invalid");
            $("#e_nombre").css({"display":"block", "color": "red"}); 
            $('#e_nombre').text('Your Name Please');
            $("#e_nombre").fadeOut("4000");
            $(this).focus();
        }
    });
    
    $('#Apellido').blur(function() {
		var is_last = nameReg.test($(this).val());
        if(is_last){
            $(this).removeClass("invalid").addClass("valid");
        }
        else{
            $(this).removeClass("valid").addClass("invalid");
            $("#e_apellido").css({"display":"block", "color": "red"}); 
            $('#e_apellido').text('Your Last Name Please');
            $("#e_apellido").fadeOut("4000");
            $(this).focus();
        }
    });
    
    $('#Email').blur(function() {
        var is_email = emailReg.test($(this).val());
        if(is_email){
            $(this).removeClass("invalid").addClass("valid");
        }
        else{
            $(this).removeClass("valid").addClass("invalid");
            $("#e_email").css({"display":"block", "color": "red"});  
            $('#e_email').text('Your valid email Please\n');
            $("#e_email").fadeOut("4000");
            $(this).focus();
        }
    });
    
    $('#Telefono').blur(function() {
		var is_number = numberReg.test($(this).val());
        if(is_number){
            $(this).removeClass("invalid").addClass("valid");
        }
        else{
            $(this).removeClass("valid").addClass("invalid");
            $("#e_telefono").css({"display":"block", "color": "red"}); 
            $('#e_telefono').text('Your valid phone number Please');
            $("#e_telefono").fadeOut("4000");
            $(this).focus();
        }
    });
});