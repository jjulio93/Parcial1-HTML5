window.onload = function(){
    $("#dialog").dialog ({
        autoOpen : false,
        buttons: [
        {
            text: "Ok",
            icon: "ui-icon-heart",
            click: function() {
                storeNickname();
                $( this ).dialog( "close" );
            }
 
            // Uncommenting the following line would hide the text,
            // resulting in the label being used as a tooltip
            //showText: false
        }
        ]
    });
    
	document.getElementById("submit-button").onclick = openDialog;
};

function openDialog(){
    
    if (typeof(Storage) !== "undefined") {
    // Code for localStorage/sessionStorage.
    var nickname = sessionStorage.getItem("nickname");
    if(nickname == null){
        $("#dialog").dialog("open");
    }
    else{
        addComment(nickname);
    }    
    } else {
        alert("Localstorage not suported");
    }
}
       
function storeNickname(){
    if (typeof(Storage) !== "undefined") {
    // Code for localStorage/sessionStorage.
    var nickname = $("#apodo").val();
    sessionStorage.setItem("nickname", nickname);
    addComment(nickname)
    } else {
        alert("Localstorage not suported");
    }
}

function addComment(nickname) {

    var whiteSpaceRegex = '^[ t]+'; 
    
    var text = document.getElementById("comment-text").value;
    var ul = document.getElementById("list");
    var li = document.createElement("li");
    var h4 = document.createElement("h4");    
    var p = document.createElement("p");
    
    if(text == ""){
        return;
    }
    
    h4.appendChild(document.createTextNode(nickname + "      " + getDateString()));
    h4.setAttribute("class", "comment-date");
    p.appendChild(document.createTextNode(text));
    p.setAttribute("class", "comment-content");   
    li.appendChild(h4);
    li.appendChild(p);
    ul.appendChild(li);
}

function getDateString(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) { 
        dd = '0'+dd
    } 

    if(mm<10) {
        mm = '0'+mm
    } 

    today = mm + '/' + dd + '/' + yyyy;
    return today;
}